# This is a sample Python script.
from os import listdir


# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

def print_table_name():
    print(
        "Заявок	Потери	Вер-ть потери	П(%)	Длина очер.	Загрузка	Ср.вр. ож.	О(%)	СКО вр.ож.	Дов. инт.	Д(%)    Ср.вр. преб.	О(%)	СКО вр. преб.	Дов. инт.	Д(%)")


def parse_file(name):
    otk1, otk2 = -1, -1
    uzel1_entries, uzel2_entries = -1, -1
    uzel1_util, uzel2_util = -1, -1
    tw1_mean, tw1_std_dev, tw2_mean, tw2_std_dev = [-1] * 4
    tu1_mean, tu1_std_dev, tu2_mean, tu2_std_dev = [-1] * 4
    buf1_ave_count, buf2_ave_count = -1, -1
    with open(name) as f:
        for line in f:
            if line.startswith("OTK_1"):
                otk1 = int(line.split()[3])
            elif line.startswith("OTK_2"):
                otk2 = int(line.split()[3])
            elif line.startswith("                   19    TERMINATE"):
                uzel2_entries = int(line.split()[2])
            elif line.startswith(" UZEL2"):
                uzel2_util = float(line.split()[2])
            elif line.startswith("                   10    TERMINATE"):
                uzel1_entries = int(line.split()[2])
            elif line.startswith(" UZEL1"):
                uzel1_util = float(line.split()[2])
            elif line.startswith(" BUF1"):
                buf1_ave_count = float(line.split()[5])
            elif line.startswith(" BUF2"):
                buf2_ave_count = float(line.split()[5])
            elif line.startswith(" TW_1"):
                tw1_mean = float(line.split()[1])
                tw1_std_dev = float(line.split()[2])
            elif line.startswith(" TW_2"):
                tw2_mean = float(line.split()[1])
                tw2_std_dev = float(line.split()[2])
            elif line.startswith(" TU_1"):
                tu1_mean = float(line.split()[1])
                tu1_std_dev = float(line.split()[2])
            elif line.startswith(" TU_2"):
                tu2_mean = float(line.split()[1])
                tu2_std_dev = float(line.split()[2])
    return (
        f"{otk1 + otk2 + uzel1_entries + uzel2_entries}\t{otk1 + otk2}\tauto\tauto\t{buf1_ave_count + buf2_ave_count}\t"
        f"{(uzel1_util + uzel2_util) / 2}\t{tw1_mean / 3 + 2 * tw2_mean / 3}\tauto\t{tw1_std_dev / 3 + 2 * tw2_std_dev / 3}\tauto\tauto\t"
        f"{tu1_mean / 3 + 2 * tu2_mean / 3}\tauto\t{tu1_std_dev / 3 + 2 * tu2_std_dev / 3}\tauto\tauto")


def parse_dir(name):
    strings = []
    for f in listdir(name):
        strings.append(parse_file(f"{name}/{f}"))
    strings.sort(key=lambda x: int(x.split()[0]))
    print(*strings, sep='\n')


def parse_dirs(*name):
    for d in name:
        print()
        print()
        print(f"Parsing {d}:")
        print_table_name()
        parse_dir(d)
        print()
        print()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # parse_dirs("var15")
    parse_dirs(*[f"var{i}" for i in range(1, 16)])

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
